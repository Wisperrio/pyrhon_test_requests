from django.shortcuts import render
from django.views import View
import requests


class News(View):

    def get(self, http_request):
        news = requests.get('https://api.kasipodaq.org/api/v1/news')
        print(news.json())
        return render(http_request, 'index.html', context={'content': news.json()})
