from django.urls import path
from request_app import views

urlpatterns = [
    path('news/', views.News.as_view(), name="news"),
]
